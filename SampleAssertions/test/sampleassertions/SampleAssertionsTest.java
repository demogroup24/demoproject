/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sampleassertions;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.jupiter.api.Disabled;

import jdk.nashorn.internal.ir.annotations.Ignore;

public class SampleAssertionsTest {

	public SampleAssertionsTest() {
	}

	@Test
	public void testAssertArrayEquals() {
		byte[] expected = "test".getBytes();
		byte[] actual = "test".getBytes();
		assertArrayEquals("failure - byte arrays not same", expected, actual);
	}

	@Test
	public void testAssertEquals() {
		assertEquals("failure - strings are not equal", "text", "text");
		// Checks that two primitives/objects are equal.
	}
 
	@Test
	public void testAssertFalse() {
		assertFalse("failure - should be false", false);
		// Checks that a condition is false.
	}

	@Test
	public void testAssertNotNull() {
		assertNotNull("should not be null", new Object());
		// Checks that an object isn't null.
	}

	@Test
	public void testAssertNotSame() {
		assertNotSame("should not be same Object", new Object(), new Object());
	}

	@Test
	public void testAssertNull() {
		assertNull("should be null", null);
		// Checks that an object is null.
	}

	@Ignore
	@Test
	public void testAssertSame() {
		Integer aNumber = Integer.valueOf(62);
		assertSame("should be same", aNumber, aNumber);
	}

	@Disabled
	@Test
	public void testAssertTrue() {
		assertTrue("failure - should be true", true);
		// Checks that a condition is true.
	}

}
